*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${BROWSER}    Chrome
${URL}    https://www.tnpublicnotice.com/(S(1it0e23cznlkh15bjlfpwtps))/default.aspx

${POPULAR_SEARCH}    name:ctl00$ContentPlaceHolder1$as1$txtExclude

${ICONE_SEARCH}    name:ctl00$ContentPlaceHolder1$as1$btnGo

${chp_CLIC_VIEW}    xpath://*[@id="ctl00_ContentPlaceHolder1_WSExtendedGridNP1_GridView1_ctl03_btnView2"]

${CLIC_CAPTCHA}    id:recaptcha-anchor

${CLIC_VIEW_NOTICE}    name:ctl00$ContentPlaceHolder1$PublicNoticeDetailsBody1$btnViewNotice


*** Test Cases ***
Naviguer vers Google et fermer le navigateur
    [Documentation]     Navigue vers tnpublicnotice
    Open Browser    ${URL}    headlesschrome
    Maximize Browser Window
    Sleep    7s
   # Click Element   ${POPULAR_SEARCH}
    Input Text    ${POPULAR_SEARCH}    vote
    Click Element    ${ICONE_SEARCH}
    Sleep     5s
    Click Element    ${chp_CLIC_VIEW}
    Sleep    7s
    
    Close Browser



